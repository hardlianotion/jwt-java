organization := "co.bascule"

name := "jwt-java"

version := "1.0-SNAPSHOT"

autoScalaLibrary := false

javacOptions ++= Seq("-source", "21.0.2", "-target", "21.0.2")

crossPaths := false

libraryDependencies ++= Seq(
  "net.aichler" % "jupiter-interface" % JupiterKeys.jupiterVersion.value % Test,
  "org.junit.jupiter" % "junit-jupiter-api" % "5.9.2" % Test,
  "io.jsonwebtoken" % "jjwt-impl" % "0.11.5",
  "io.jsonwebtoken" % "jjwt-jackson" % "0.11.5",
  "io.jsonwebtoken" % "jjwt-api" % "0.11.5",
  "org.bouncycastle" % "bcprov-jdk18on" % "1.77"
)

