import static org.junit.jupiter.api.Assertions.*;

import java.security.*;
import org.junit.jupiter.api.*;

import java.util.Objects;
import java.util.UUID;
import java.security.interfaces.*;

class TokenRoundTripTests {

  ECPublicKey publicKey;
  ECPrivateKey privateKey;

  @BeforeEach
  void generateKeys() {
    KeyPair pair = TokenExample.createEncryptionKeys();
    publicKey = (ECPublicKey) Objects.requireNonNull(pair).getPublic();
    privateKey = (ECPrivateKey) pair.getPrivate();
  }

  @Test
  void generatedTokenIsValid() {
    UUID userId = UUID.randomUUID();
    String token = TokenExample.createToken(userId, privateKey);

    assertTrue(TokenExample.validateToken(token, publicKey));
  }

  @Test
  void validatePublicKeyStr() {
    String publicKeyStr = TokenExample.publicKeytoString(publicKey);
    ECPublicKey recoverPublicKey = TokenExample.publicKeyFromString(publicKeyStr);

    //check by trying to validate private key
    UUID userId = UUID.randomUUID();
    String token = TokenExample.createToken(userId, privateKey);

    assertTrue(TokenExample.validateToken(token, recoverPublicKey));
  }

  @Test
  void cannotValidateWithKeyPair() {
    var wrongKeyPair = TokenExample.createEncryptionKeys();
    var wrongPublicKey = (ECPublicKey) Objects.requireNonNull(wrongKeyPair).getPublic();

    //check by trying to validate private key
    UUID userId = UUID.randomUUID();
    String token = TokenExample.createToken(userId, privateKey);

    assertFalse(TokenExample.validateToken(token, wrongPublicKey));
  }
}
