/**
 * Going to encrypt the Jwt token with header
 * {"alg":"ES256"}
 *
 * and claim
 * {"sub":"<USER_UUID>",
 * "iat":"<TOKEN_CREATION_TIME>",
 * "exp":"<TOKEN_CREATION_TIME+24 HOURS>"}
 *
 * reference https://github.com/jwtk/jjwt
 */

import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;

import io.jsonwebtoken.*;
import io.jsonwebtoken.SignatureAlgorithm;

import java.math.BigInteger;
import java.security.*;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPublicKeySpec;
import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;


public class TokenExample {

  static String ALGORITHM = "ES256";
  static String PROVIDER = "BC";
  static String ECDSA = "ECDSA";
  static String STD_NAME = "P-256";

  /**
   * This looks unused but is necessary - side-effects of this function include
   * registering the Bouncy Castle provider.
   */
  private static final int BC_PREF = Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
  //this is the client's key pair.  Generated once.
  private final static KeyPair KEY_PAIR = createEncryptionKeys();
  //this is passed to bascule server at registration time.
  static ECPublicKey PUBLIC_KEY = (ECPublicKey) Objects.requireNonNull(KEY_PAIR).getPublic();

  //this is kept on the client and not shared.
  static ECPrivateKey PRIVATE_KEY = (ECPrivateKey) KEY_PAIR.getPrivate();

  private final static ECNamedCurveParameterSpec PRIME_256_V1_PARAMS = ECNamedCurveTable.getParameterSpec(STD_NAME);
  private final static ECNamedCurveSpec PRIME_256_V1_SPEC = new ECNamedCurveSpec(STD_NAME,
      PRIME_256_V1_PARAMS.getCurve(),
      PRIME_256_V1_PARAMS.getG(), PRIME_256_V1_PARAMS.getN(),
      PRIME_256_V1_PARAMS.getH());

  static KeyPair createEncryptionKeys() {
    try {
      ECGenParameterSpec spec = new ECGenParameterSpec(STD_NAME);
      KeyPairGenerator generator = KeyPairGenerator.getInstance(ECDSA, PROVIDER);
      generator.initialize(spec, new SecureRandom());
      return generator.generateKeyPair();
    } catch (GeneralSecurityException error) {
      //handling required by KeyPairGenerator.getInstance,
      //presumably we can do better than this.
      System.out.println("NOT AGAIN: "  + error.getMessage());
      return null;
    }

  }

  static String publicKeytoString(ECPublicKey publicKey) {
    return publicKey.getW().getAffineX().toString() + ":" + publicKey.getW().getAffineY().toString();
  }
  //server will pass a public key string.
  // This is how to convert the string into a public key.
  static ECPublicKey publicKeyFromString(String keyStr) {
    String[] wBits = keyStr.split(":");

    if  (wBits.length != 2) {
      throw new RuntimeException("invalid public key format.");
    }
    BigInteger[] publicKeyData = new BigInteger[2];
    publicKeyData[0] = new BigInteger(wBits[0]);
    publicKeyData[1] = new BigInteger(wBits[1]);

    ECPublicKeySpec spec = new ECPublicKeySpec(new ECPoint(publicKeyData[0], publicKeyData[1]), PRIME_256_V1_SPEC);
    try {
      return (ECPublicKey) KeyFactory.getInstance (ECDSA, PROVIDER).generatePublic(spec);
    } catch (GeneralSecurityException error) {
      //handling required by KeyPairGenerator.getInstance,
      //presumably we can do better than this.
      System.out.println("NOT AGAIN: "  + error.getMessage());
      return null;
    }
  }

  static String createToken(UUID userUUID, ECPrivateKey privateKey) {
    Instant now = Instant.now();

    return Jwts.builder()
        .setIssuedAt(Date.from(now))
        .setSubject(userUUID.toString())
        .setExpiration(Date.from(now.plusSeconds(24*60*60)))
        .signWith(privateKey,  SignatureAlgorithm.ES256).compact();

  }

  //every message that comes from the bascule server should be signed with a JwtToken.
  //(this is not yet implemented).
  //this is how you validate the token  passed by the server.
  static boolean validateToken(String token, ECPublicKey publicKey) {
    Jws<Claims> jws;

    try {
      jws = Jwts.parser()         // (1)
          .setSigningKey(publicKey)         // (2)
          .parseClaimsJws(token); // (3)

      // we can safely trust the JWT
      return true;

    } catch (JwtException error) {       // (4)

      // we *cannot* use the JWT as it is invalid.
      return false;
    }
  }
}

