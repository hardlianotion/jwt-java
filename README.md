# jwt-java

tests jwt framework for token creation

This is a demo of token creation using BouncyCastle encryption framework for a java client.  To run the tests, 
run the command:

`sbt test`
